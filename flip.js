const output_area = this;

(function() {
    "use strict";

    const flip_id = '$FLIP_ID';
    const frontend_id = '$FRONTEND_ID';
    const n_pages = $N_PAGES;
    let visible_ix = $START_INDEX;
    let desired_ix = visible_ix;
    let expecting_update = false;
    let page_cache = new Map();
    
    const pageno_el = document.getElementById(`${frontend_id}-pageno`);
    const comm = Jupyter.notebook.kernel.comm_manager.new_comm('flipbook');
    
    function show_page(data) {
        // Borrowed from OutputArea clear_output:
        // Lock the height while the content changes to avoid jumps
        const height = output_area.element[0].getBoundingClientRect().height;
        output_area.element.height(height);
        data.result.transient = {display_id: `${frontend_id}-out`};
        output_area.update_display_data(
            data.result, _ =>output_area.element.height('')
        );
        
        pageno_el.textContent = data.pageno;
        visible_ix = data.index;
        if (visible_ix === desired_ix) {
            expecting_update = false;
            prefetch();
        } else {
            request_page(desired_ix);
        }
    }
    function prefetch() {
        (visible_ix > 0) && request_page(visible_ix - 1);
        (visible_ix + 1 < n_pages) && request_page(visible_ix + 1);
    }
    
    function to_page(new_index) {
        desired_ix = new_index;
        if (page_cache.has(desired_ix)) {
            show_page(page_cache.get(desired_ix));
        } else if (!expecting_update) {
            expecting_update = true;
            request_page(desired_ix);
        }
    }
    function prev_page() {
        if (desired_ix > 0) {
            to_page(desired_ix - 1);
        }
    }
    function next_page() {
        if (desired_ix < n_pages - 1) {
            to_page(desired_ix + 1);
        }
    }
    
    function request_page(i) {
        if (!page_cache.has(i)) {
            comm.send({flip_id: flip_id, index: i});
        }
    }

    comm.on_msg(msg => {
        const data = msg.content.data;
        //console.log(`Got page for ${data.index}`)
        if (visible_ix !== desired_ix) {
            show_page(data);
        }
        // Put the page in the cache, then trim the cache so we only store 3 entries
        page_cache.set(data.index, data);
        page_cache = new Map(function* () {
            for (const [ix, data] of page_cache) {
                if ((ix >= desired_ix - 1) && (ix <= desired_ix + 1)) {
                    yield [ix, data];
                }
            }
        }()); 
    });
    prefetch();
    
    document.getElementById(`${frontend_id}-controls`).addEventListener('keydown', evt => {
        switch (evt.key) {
            case "ArrowLeft":
                prev_page();
                evt.preventDefault();
                break;
            case "ArrowRight":
                next_page();
                evt.preventDefault();
                break;
            case "ArrowUp":
                document.activeElement.blur();
                return;
            case "ArrowDown":
                document.activeElement.blur();
                return;
            case "Escape":
                document.activeElement.blur();
                return;
        }
    });
    
    document.getElementById(`${frontend_id}-prev`).addEventListener(
        'click', evt => prev_page()
    );
    document.getElementById(`${frontend_id}-next`).addEventListener(
        'click', evt => next_page()
    );
})();
