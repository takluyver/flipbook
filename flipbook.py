from collections.abc import Mapping
from itertools import count
from pathlib import Path
from string import Template
import traceback

from IPython import get_ipython
from IPython.display import HTML, Javascript, display

indir = Path(__file__).parent
js_file = indir / 'flip.js'
html_file = indir / 'controls.html'


flip_ids = count()
frontend_ids = count()
flip_register = {}

last_error = None

def show_error():
    """Show last error from handling comm messages"""
    if last_error is not None:
        traceback.print_exception(type(last_error), last_error, last_error.__traceback__)

def fbk_comm_open(comm, open_msg):

    # Register handler for later messages
    @comm.on_msg
    def _recv(msg):
        global last_error
        try:
            data = msg['content']['data']
            flip = flip_register[data['flip_id']]
            comm.send(flip.req(data['index']))
        except Exception as e:
            last_error = e

_ip = get_ipython()
if _ip and getattr(_ip, 'kernel', None):
    _ip.kernel.comm_manager.register_target('flipbook', fbk_comm_open)

class Flip:
    def __init__(self, data, pages=None, start_at=None):
        if pages is None:
            if callable(data):
                raise TypeError("callable without pages specified")
            elif isinstance(data, Mapping):
                pages = list(data.keys())
            else:
                pages = range(len(data))
        self.data = data
        self._get_page = data if callable(data) else data.__getitem__
        self.pages = pages
        self._start_idx = 0 if (start_at is None) else self.pages.index(start_at)
        self._id = f'flipbook-{next(flip_ids)}'
        flip_register[self._id] = self

    def req(self, index):
        if index < 0:
            return
       
        page = self.pages[index]
        value = self._get_page(page)
        
        data, metadata = get_ipython().display_formatter.format(value)
        return {
            'result': {
                'data': data,
                'metadata': metadata,
            },
            'pageno': str(page),
            'index': index,
        }

    def _get_html(self, frontend_id):
        return Template(html_file.read_text()).substitute({
            'ID': frontend_id,
            'START_PAGENO': self.pages[self._start_idx],
        })
    
    def _get_js(self, frontend_id):
        return Template(js_file.read_text()).safe_substitute({
            'FLIP_ID': self._id,
            'FRONTEND_ID': frontend_id,
            'N_PAGES': len(self.pages),
            'START_INDEX': self._start_idx,
        })

    def _ipython_display_(self):
        page = self.pages[self._start_idx]
        value = self._get_page(page)
        frontend_id = f'flipbook-{next(frontend_ids)}'
        display(value, display_id=frontend_id+'-out')
        display(HTML(self._get_html(frontend_id)), display_id=frontend_id+'-controls')
        display(Javascript(self._get_js(frontend_id)))
