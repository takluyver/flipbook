.. image:: https://mybinder.org/badge_logo.svg
 :target: https://mybinder.org/v2/gl/takluyver%2Fflipbook/HEAD?filepath=Demo.ipynb

Flipbook is a tool to browse through a sequence of 'pages' as output in a
Jupyter notebook.

.. image:: screenshot.png

A page can be any displayable data, such as an image or a table.
One page either side of the current one is precomputed to make turning the page
fast.

This is an experiment, built on the classic notebook APIs (not Jupyterlab).
You're very welcome to play with it, but don't expect something polished or
complete.
